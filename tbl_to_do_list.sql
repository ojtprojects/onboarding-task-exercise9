-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 02, 2024 at 05:40 AM
-- Server version: 10.4.18-MariaDB
-- PHP Version: 8.0.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `to_do_list`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_to_do_list`
--

CREATE TABLE `tbl_to_do_list` (
  `id` int(11) NOT NULL,
  `task_title` varchar(255) DEFAULT NULL COMMENT 'input task title',
  `task_name` varchar(255) DEFAULT NULL COMMENT 'input task name',
  `time` datetime NOT NULL COMMENT 'input time',
  `status` enum('Inprogress','Done') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_to_do_list`
--

INSERT INTO `tbl_to_do_list` (`id`, `task_title`, `task_name`, `time`, `status`) VALUES
(1, 'TEST', 'PHP BACKEND API', '2024-11-11 13:23:44', 'Inprogress'),
(2, '2TEST', 'PHP BACKEND API', '2024-11-11 13:23:44', 'Done');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_to_do_list`
--
ALTER TABLE `tbl_to_do_list`
  ADD PRIMARY KEY (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
