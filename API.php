<?php
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Credentials: true");
header("Access-Control-Allow-Methods: POST, GET, PUT, DELETE");
header("Access-Control-Allow-Headers: Content-Type");
require_once('MysqliDb.php');

class API {
    private $db;

    public function __construct()
    {
        $this->db = new MysqliDb('localhost', 'root', '', 'to_do_list');
    }

    /**
     * HTTP GET Request
     *
     * @param $payload
     */
   
     public function httpGet($payload)
     {  
       
        if (!is_array($payload)) {
            echo $this->failedResponse('GET', 'failed', 'The payload should be an array');
        }
       
        if (empty($payload)) {
            $result = $this->db->get('tbl_to_do_List');
        } else {
          
            if (isset($payload['id'])) {
                $this->db->where('id', $payload['id']);
            } elseif (isset($payload['status'])) {
                $this->db->where('status', $payload['status']);
            } else {
                echo $this->failedResponse('GET', 'failed', 'Failed Fetch Request');
                return;
            }
        
            $result = $this->db->get('tbl_to_do_List');
        }
        
        if ($result) {
            echo $this->successResponse('GET', 'success', $result);
        } else {
            echo $this->failedResponse('GET', 'failed', 'Failed Fetch Request');
        }

        
    }
      
    

    
    
    /**
     * HTTP POST Request
     *
     * @param $payload
     */
    public function httpPost($payload)
    {   
        // check if payload is an array or if it is an array and empty. This gets the first element of the payload array and assigns it to the $payload variable.
        if (!is_array($payload)) {
            echo $this->failedResponse('POST', 'failed', 'The payload should be an array');
        } elseif (is_array($payload) && empty($payload)) {
            echo $this->failedResponse('POST', 'failed', 'The payload should be an array and not empty');
        } else {
            $result = $this->db->insert('tbl_to_do_List', $payload);
    
            if ($result) {
                echo $this->successResponse('POST', 'success', $payload);
            } else {
                echo $this->failedResponse('POST', 'failed', 'Failed to Insert Data');
            }
        }
       
    }

    /**
     * HTTP PUT Request
     *
     * @param $id
     * @param $payload
     */
    public function httpPut($id, $payload)
    {
       
        if (!is_int($id) && !is_string($id) || empty($id)) {
            echo $this->failedResponse('PUT', 'failed', 'No ID being passed');
            return;
        }
    
        if (empty($payload)) {
            echo $this->failedResponse('PUT', 'failed', 'Payload is empty');
            return;
        }
    
        $validColumns = ['status', 'task_title', 'task_name', 'time'];
        $allowedPayload = array_intersect_key($payload, array_flip($validColumns));
    
        if (!empty($allowedPayload)) {
            $this->db->where('id', $id);
            $result = $this->db->update('tbl_to_do_List', $allowedPayload);
    
            if ($result) {
                echo $this->successResponse('PUT', 'success', $allowedPayload);
            } else {
                echo $this->failedResponse('PUT', 'failed', 'Failed to Update Data');
            }
        } else {
            echo $this->failedResponse('PUT', 'failed', 'Invalid payload for update');
        }
    }
    /**
     * HTTP DELETE Request
     *
     * @param $id
     * @param $payload
     */
    public function httpDelete($id, $payload)
    {
      
        if (empty($id)) {
            echo $this->failedResponse('DELETE', 'failed', 'No ID passed');
            return;
        }
    
        if (empty($payload)) {
            echo $this->failedResponse('DELETE', 'failed', 'Empty payload');
            return;
        }
    
        if (is_array($payload)) {
            if ($id != $payload['id']) {
                echo $this->failedResponse('DELETE', 'failed', 'ID not found in payload');
                return;
            }
            $this->db->where('id', $payload, 'IN');
        }else {
            if($id != $payload){
                echo $this->failedResponse('DELETE', 'failed', 'ID not found in payload');
                return;
            }
            $this->db->where('id', $id);
        }
    
        $result = $this->db->delete('tbl_to_do_List');
    
        if ($result) {
            echo $this->successResponse('DELETE', 'success', $id);
        } else {
            echo $this->failedResponse('DELETE', 'failed', 'Failed to Delete Data');
        }
    }


    public function successResponse($method ,$status, $data = [])
    {
        http_response_code(200);
        return json_encode([
            'method' => $method,
            'status'  => $status,
            'data'    => $data,
        ]);
        
    }

    public function failedResponse($method ,$status, $message = '' )
    {
        http_response_code(400);
        return  json_encode([
            'method' => $method,
            'status'  => $status,
            'message' => $message,
           
        ]);
       
    }

}


$request_method = $_SERVER['REQUEST_METHOD'];

if ($request_method === 'GET') {
    $received_data = $_GET;
} else if ($request_method === 'POST') {
    $received_data = json_decode(file_get_contents('php://input'), true);
} else {
    if ($request_method === 'PUT' || $request_method === 'DELETE') {
        $request_uri = $_SERVER['REQUEST_URI'];
        $ids = null;
    
        $exploded_request_uri = array_values(explode("/", $request_uri));
       
        $last_index = count($exploded_request_uri) - 1;
     
        $ids = $exploded_request_uri[$last_index];
        
        $received_data = json_decode(file_get_contents('php://input'), true);

    }

    $received_data = json_decode(file_get_contents('php://input'), true);
   
}
$api = new API;

switch ($request_method) {
    case 'GET':
        $api->httpGet($received_data);
        break;
    case 'POST':
        $api->httpPost($received_data);
        break;
    case 'PUT':
        $api->httpPut($ids, $received_data);
        break;
    case 'DELETE':
        $api->httpDelete($ids, $received_data);
        break;
}
?>