<?php


namespace Tests\Api;

use Tests\Support\ApiTester;

class TodoListCest
{
    
    public function _before(ApiTester $I)
    {
    }

       
    //TESTS
   
        //INSERT
             //   SUCESSS
            public function iShouldInsertDataSuccess(ApiTester $I)
            {
                $I->haveHttpHeader('Content-Type', 'application/json');
                $I->sendPost('/exercise9/API.php/', [
                     
                        'id' => 1,
                        'task_title'=> 'Insert API TEST',
                        'task_name' => 'PHP BACKEND API 9',
                        'time'=> "2024-03-21T12:00:00",
                        'status' => 'Done' 
                    
                ]);
                $I->seeResponseCodeIs(200);
                $I->seeResponseIsJson();
                $I->seeResponseContainsJson(['status' => 'success']);        
            }       
            
            
        //GET
            // SUCCESS
           public function iShouldGetDataSuccess(ApiTester $I)
           {
               $I->haveHttpHeader('Content-Type', 'application/json');
               $I->sendGet('/exercise9/API.php/',[
                'status' => 'Inprogress'
                ] 
            );
               $I->seeResponseCodeIs(200);
               $I->seeResponseIsJson();
               $I->seeResponseContainsJson(['status' => 'success']);
           }
            

        // UPDATE 
            //SUCCESS
            public function iShouldUpdateDataSuccess(ApiTester $I)
            {
                $I->haveHttpHeader('Content-Type', 'application/json');
                    $I->sendPut('/exercise9/API.php/2', [
                    
                        'task_title' => "EDIT API TEST",
                        'task_name' => 'PHP BACKEND API 9',
                        'time'=> "2024-10-10T10:23:44",
                        'status' => 'Inprogress'
                    
                    ]);
                    $I->seeResponseCodeIs(200);
                    $I->seeResponseIsJson();
                    $I->seeResponseContainsJson(['status' => 'success']);
            }
        //DELETE
            //SUCCESS 
        
            //PAYLOAD WITH ARRAY 
            public function iShouldDeleteDataSuccessArray(ApiTester $I)
            {
                $I->haveHttpHeader('Content-Type', 'application/json');
                $I->sendDelete('/exercise9/API.php/5', [
                        'id' => 5,
                        'task_title'=> 'Insert API TEST',
                        'task_name' => 'PHP BACKEND API 9',
                        'time'=> "2024-03-21T12:00:00",
                        'status' => 'Done'    
                ]);
                $I->seeResponseCodeIs(200);
                $I->seeResponseIsJson();
                $I->seeResponseContainsJson(['status' => 'success']);
            }
            
            //PAYLOAD WITH NO ARRAY 
            public function iShouldDeleteDataSuccessNoArray(ApiTester $I)
            {
                $I->haveHttpHeader('Content-Type', 'application/json');
                $I->sendDelete('/exercise9/API.php/3', ['id' => 3]);
                $I->seeResponseCodeIs(200);
                $I->seeResponseIsJson();
                $I->seeResponseContainsJson(['status' => 'success']);
            }
}

