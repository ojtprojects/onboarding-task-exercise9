<?php


namespace Tests\Api;

use Tests\Support\ApiTester;

class TodoListFailCest
{
    public function _before(ApiTester $I)
    {
    }

    //TESTS
   
        //INSERT
           
          
       // FAILS
            //EMPTY PAYLOAD

            public function iShouldInsertDataFailEmpty(ApiTester $I)
                {
                    $I->haveHttpHeader('Content-Type', 'application/json');
                    $I->sendPost('/exercise9/API.php/', [
                       
                    ]);
                    $I->seeResponseCodeIs(400);
                    $I->seeResponseIsJson();
                    $I->seeResponseContainsJson(['status' => 'failed']);

            }
        
            // //PAYLOAD NOT ARRAY
            public function iShouldInsertDataFailNotArray(ApiTester $I)
                {
                    $I->haveHttpHeader('Content-Type', 'application/json');
                    
                    $I->sendPost('/exercise9/API.php/', "test");
                    $I->seeResponseCodeIs(400);
                    $I->seeResponseIsJson();
                    $I->seeResponseContainsJson(['status' => 'failed']);

            }
            
            // // Failed to Insert Data   
            public function iShouldInsertDataFail(ApiTester $I)
                {
                    $I->haveHttpHeader('Content-Type', 'application/json');
                    $I->sendPost('/exercise9/API.php/', [
                        
                            'id' => 1,
                            'task_title'=> 'Insert API TEST',
                            'task_name' => 'PHP BACKEND API 9',
                            'time'=> "2024-03-21T12:00:00",
                            'status' => 'Done' 
                        
                    ]);
                    $I->seeResponseCodeIs(400);
                    $I->seeResponseIsJson();
                    $I->seeResponseContainsJson(['status' => 'failed']);

            }
         
            


            
    //GET
            //FAILS

            //Invalid payload
            public function iShouldGetDataFailPayload(ApiTester $I)
            {
                    $I->haveHttpHeader('Content-Type', 'application/json');
                    $I->sendGet('/exercise9/API.php/', [
                      'check' => "testing"
                    ]
                    );
                    $I->seeResponseCodeIs(400);
                    $I->seeResponseIsJson();
                    $I->seeResponseContainsJson(['status' => 'failed']);
            }
          
            //Failed Fetch Request
            public function iShouldGetDataFail(ApiTester $I)
            {
                    $I->haveHttpHeader('Content-Type', 'application/json');
                    $I->sendGet('/exercise9/API.php/', [
                    'status' => "testing"
                    ]
                    );
                    $I->seeResponseCodeIs(400);
                    $I->seeResponseIsJson();
                    $I->seeResponseContainsJson(['status' => 'failed']);
            }







    // UPDATE 
           
        //FAILS  

            //EMPTY ID
            public function iShouldUpdateDataFailEmptyID(ApiTester $I)
            {
                $I->haveHttpHeader('Content-Type', 'application/json');
                $I->sendPut("/exercise9/API.php/", [
                    'task_title' => "EDIT API TEST",
                    'task_name' => 'PHP BACKEND API 9',
                    'time' => "2024-10-10T10:23:44",
                    'status' => 'Inprogress'
                ]);
            
                $I->seeResponseCodeIs(400);
                $I->seeResponseContainsJson(['status' => 'failed']);
                $I->seeResponseContainsJson(['message' => 'No ID being passed']);
            }
            
            //EMPTY PAYLOAD
            public function iShouldUpdateDataFailEmptyPayload(ApiTester $I)
            {
                $I->haveHttpHeader('Content-Type', 'application/json');
                    $I->sendPut('/exercise9/API.php/2', [
                    
                    ]);
                    $I->seeResponseCodeIs(400);
                    $I->seeResponseIsJson();
                    $I->seeResponseContainsJson(['status' => 'failed']);
            }
            // Invalid Payload
                public function iShouldUpdateDataFail(ApiTester $I)
            {
                $I->haveHttpHeader('Content-Type', 'application/json');
                    $I->sendPut('/exercise9/API.php/2', [
                     
                        'check' => 'InProgress'
                    
                    ]);
                    $I->seeResponseCodeIs(400);
                    $I->seeResponseIsJson();
                    $I->seeResponseContainsJson(['status' => 'failed']);
            }



    //DELETE

        //FAILS  
            //NO ID

            public function iShouldDeleteDataFailNoID(ApiTester $I)
            {
                $I->haveHttpHeader('Content-Type', 'application/json');
                $I->sendDelete('/exercise9/API.php/', [
                    2
                ]);
                $I->seeResponseCodeIs(400);
                $I->seeResponseIsJson();
                $I->seeResponseContainsJson(['status' => 'failed']);         
           }

            // NO PAYLOAD
            public function iShouldDeleteDataFailEmptyPayload(ApiTester $I)
            {
                    $I->haveHttpHeader('Content-Type', 'application/json');
                    $I->sendDelete('/exercise9/API.php/1', [
                        
                    ]);
                    $I->seeResponseCodeIs(400);
                    $I->seeResponseIsJson();
                    $I->seeResponseContainsJson(['status' => 'failed']);
            }  

            // Id Mismatch
            public function iShouldDeleteDataFail(ApiTester $I)
            {
                $I->haveHttpHeader('Content-Type', 'application/json');
                $I->sendDelete('/exercise9/API.php/7', ['id' => 3]
                    
                );
                $I->seeResponseCodeIs(400);
                $I->seeResponseIsJson();
                $I->seeResponseContainsJson(['status' => 'failed']);
            }   
}
